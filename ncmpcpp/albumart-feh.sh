#!/bin/zsh

MUSIC_DIR=/run/media/tch0wk/fa1eccc1-ebff-47ea-a021-a9b93e5febea/Music/
COVER=/tmp/cover.jpg

{
	album="$(mpc --format %album% current)"
	file="$(mpc --format %file% current)"
	album_dir="${file%/*}"
	[[ -z "$album_dir" ]] && exit 1
	album_dir="$MUSIC_DIR/$album_dir"

	covers="$(find "$album_dir" -type d -exec find {} -maxdepth 1 -type f -iregex ".*/.*\(${album}\|cover\|front\).*[.]\(jpe?g\|png\|gif\|bmp\)" \; )"
	src="$(echo -n "$covers" | head -n1)"
	rm -f "$COVER"
	if [[ -n "$src" ]] ; then
		cp "$src" "$COVER"
		if [[ -f "$COVER" ]] ; then
			killall -q feh
			feh -B black -q -. -N -g 590x590+2827+9 -^ art "$COVER" #1440p
			# feh -B black -q -. -N -g 390x390+1505+27 -^ art "$COVER" #1080p
		fi
	else
		killall -q feh
		#feh -B black -q -. -g 350x350+1455+24 -^ art "/home/tch0wk/.config/ncmpcpp/error404.png"
	fi
} &
