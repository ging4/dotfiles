#!/bin/sh

MUSIC_DIR=/run/media/tch0wk/e82e54c4-75fc-4d18-9bb1-d5a6aea3b32a/Music
COVER=/tmp/cover.jpg

{
	album="$(mpc --format %album% current)"
	file="$(mpc --format %file% current)"
	album_dir="${file%/*}"
	[[ -z "$album_dir" ]] && exit 1
	album_dir="$MUSIC_DIR/$album_dir"

	covers="$(find "$album_dir" -type d -exec find {} -maxdepth 1 -type f -iregex ".*/.*\(${album}\|cover\|front\).*[.]\(jpe?g\|png\|gif\)" \;)"
	src="$(echo -n "$covers" | head -n1)"

	if [[ -n "$src" ]]; then
		rsync -aq "$src" "$COVER"
		if [[ -f "$COVER" ]] && ! pgrep -x "nsxiv" >/dev/null; then
			nsxiv -b -s F "$COVER"
		fi
	fi
} &
