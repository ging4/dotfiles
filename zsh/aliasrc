#!/bin/sh

alias ka="killall"
alias ar2="aria2c -x16"

alias yt="yt-dlp -ic"
alias yta="yt-dlp -xic" # Download only audio

alias tl='tmux ls'
alias ta='tmux a -t'
alias ts='tmux new -s'
alias tk='tmux kill-server'

alias icat="kitty +kitten icat"

alias ls='exa --long --icons --git --sort=size'
alias lsa='exa --long --icons --git --sort=size --all'
alias lst='exa --long --icons --git --sort=size --tree'
alias grep="rg" # ripgrep alias

alias dusize='du -hsx ./* | sort -rh | head -10' # List folders by their size
alias shotty='selection=$(hacksaw) && shotgun -g $selection - | xclip -t "image/png" -selection clipboard'

alias ip='ip --color'
alias ipb='ip --color --brief'
alias ipp="curl http://whatismyip.akamai.com/"
alias weather="curl https://wttr.in"
alias weatherv2="curl https://v2.wttr.in"

alias gtun='mpv "/home/tch0wk/Pictures/Useful/afinador guitarra.webm"'
alias tcd='cd /home/tch0wk/Documents/Tabs/'
alias scd='cd /home/tch0wk/Documents/Scripts/'
alias ucd='cd /home/tch0wk/Documents/University/'
alias wcd='cd /home/tch0wk/Documents/Workshop/'
alias ncd='cd /home/tch0wk/Documents/Notes/'
alias lcd='cd /srv/http/'
alias gcd='cd /home/tch0wk/Documents/Workshop/dotfiles/'

alias heic2jpg='find . -name "*.heic" -exec heif-convert {} {}hevc.jpg \;'

alias pachealth='sudo paccheck --list-broken --files'
alias pacdiffnvim='SUDO_EDITOR="nvim -d" DIFFPROG="sudo -e" pacdiff' # sudoedit with nvim in diff mode only when using pacdiff
alias vimdiff='nvim -d'

alias xamppStarto='sudo systemctl stop mariadb.service ; sudo systemctl stop lighttpd.service ; sudo xampp start'
alias xamppStoppu='sudo xampp stop ; sudo systemctl start mariadb.service; sudo systemctl start lighttpd.service'

alias compilTmp='sudo mount -o remount,size=24G,noatime /tmp'
alias normalTmp='sudo mount -o remount,size=2G,noatime /tmp'

#alias xamppPacnew='
#    sudo find -L '/opt/lampp/' -name '*.pacnew' | while read _fnew_; do \
#      _fold_="`echo "${_fnew_}" | sed 's/\.pacnew$//'`"; \
#      sudo rm -R "${_fold_}"; \
#      sudo mv "${_fnew_}" "${_fold_}"; \
#    done

function eisvogel(){
    FILENAME=$1
    OUTFILE="${FILENAME%.*}"
    pandoc "$FILENAME" -o $OUTFILE.pdf --from markdown --template /usr/share/pandoc/data/templates/eisvogel.latex --listings --pdf-engine=xelatex
}

# upscale all $1 type images on folder
function waifu2x() {
	echo "Input: $1"
	find . -name "*.$1" -exec waifu2x-ncnn-vulkan -i {} -o {}waifu2x.png \;
}

# upscale all $1 type images on folder
function resrgan() {
	echo "Input: $1"
	find . -name "*.$1" -exec realesrgan-ncnn-vulkan -f jpg -i {} -o {}  \;
}

# transcode from $1 to mp3v0 (all $1 type files on folder)
function mp3v0() {
	echo "Input: $1"
	find . -name "*.$1" -exec ffmpeg -i {} -c:a libmp3lame -q:a 0 -map_metadata 0 -id3v2_version 3 -write_id3v1 1 {}.mp3 \;
}

# transcode from $1 to mp3v0 (all $1 type files on folder)
function opusvbr() {
	echo "Input: $1"
	find . -name "*.$1" -exec ffmpeg -i {} -c:a libopus -q:a 0 -map_metadata 0 -id3v2_version 3 -write_id3v1 1 {}.opus \;
}

# transcode from $1 to hevc10bit using cpu (all $1 type files on folder)
function hevc10bit-cpu() {
	echo "Input: $1"
	if [ $1 = "wmv" ]; then
		find . -name "*.$1" -exec ffmpeg -i {} -c:v libx265 -crf 23 -preset medium -pix_fmt yuv420p12le -c:a libmp3lame -q:a0 {}hevc.mp4 \;
	else
		find . -name "*.$1" -exec ffmpeg -i {} -c:v libx265 -crf 23 -preset medium -pix_fmt yuv420p12le -c:a copy {}hevc.mp4 \;
	fi
}

# transcode from $1 to hevc10bit at $2 bitrate using VAAPI (all $1 type files on folder)
function hevc10bit-gpu() {
	echo "Input: $1"
	echo "Quality: $2"
	if [ $1 = "wmv" ]; then
		# apply eq before any other filters like this: -vf 'eq=contrast=1.05:gamma=0.75:saturation=1.1,format=nv12,hwupload'
		# bitdepth: -pix_fmt yuv420p12le para 12bit
		find . -name "*.$1" -exec ffmpeg -vaapi_device /dev/dri/renderD128 -i {} -vf 'format=nv12,hwupload' -map 0:0 -c:v hevc_vaapi -map 0:a -c:a libmp3lame -q:a -rc_mode CQP -global_quality $2 -profile:v main -v verbose {}hevc.mp4 \;
	else
		find . -name "*.$1" -exec ffmpeg -vaapi_device /dev/dri/renderD128 -i {} -vf 'format=nv12,hwupload' -map 0:0 -c:v hevc_vaapi -map 0:a -c:a copy -rc_mode CQP -global_quality $2 -profile:v main -v verbose {}hevc.mp4 \;
	fi
}

# custom cd
function cd() {
	new_directory="$*"
	if [ $# -eq 0 ]; then
		new_directory=${HOME}
		builtin cd "${new_directory}"
	else
		builtin cd "${new_directory}" && ls --all
	fi
}

# remove latex junk
function texclear() {
	case "$1" in
	*.tex)
		file=$(readlink -f "$1")
		dir=$(dirname "$file")
		base="${file%.*}"
		find "$dir" -maxdepth 1 -type f -regextype gnu-awk -regex "^$base\\.(4tc|xref|tmp|pyc|pyo|fls|vrb|fdb_latexmk|bak|swp|aux|log|synctex\\(busy\\)|lof|lot|maf|idx|mtc|mtc0|nav|out|snm|toc|bcf|run\\.xml|synctex\\.gz|blg|bbl)" -delete
		;;
	*) printf "Give .tex file as argument.\\n" ;;
	esac
}

# streamlink shortcuts $2 lets you select video quality at the end of the command
function stlk() {
	case "$1" in
	aris)
		streamlink --twitch-low-latency https://www.twitch.tv/avoidingthepuddle $2
		;;
	obama)
		streamlink --twitch-low-latency https://www.twitch.tv/majinobama $2
		;;
	mason)
		streamlink --twitch-low-latency https://www.twitch.tv/masondota2 $2
		;;
	jiyuna)
		streamlink --twitch-low-latency https://www.twitch.tv/animeilluminati $2
		;;
	drqb)
		streamlink --twitch-low-latency https://www.twitch.tv/dontragequitbro $2
		;;
    hob)
		streamlink --twitch-low-latency https://www.twitch.tv/the_Happy_hob $2
		;;
    ame)
		streamlink --twitch-low-latency https://www.twitch.tv/watsonamelia_hololiveen $2
		;;
	*)
		streamlink --twitch-low-latency https://www.twitch.tv/$1 $2
		;;
	esac
}

# acestream shortcuts
function acestr() {
	case "$1" in
	fox1)
		acestream-launcher acestream://c9c062941fbecb16ce29437f346ea7177d720898 -p mpv
		;;
	bein)
		acestream-launcher acestream://bba1905fcd1c4975aec544047bf8e4cd23ce3fe0 -p mpv
		;;
	abc)
		acestream-launcher acestream://11605a4f509379f677e5f6c90f0a6211e08fcbcb -p mpv
		;;
	movistar)
		acestream-launcher acestream://3e17ba9f230b64dbaa6538b6d36b85cc3f4cd59e -p mpv
		;;
	nhk)
		acestream-launcher acestream://5f373a35bf81b99a0b7ca7148c6342835493d612 -p mpv
		;;
	xi)
		acestream-launcher acestream://77fb776f75c86f531e24f9e82958213a7ef107e8 -p mpv
		;;
	xg)
		acestream-launcher acestream://97410eabe91bb34194e78a117ef0d1ad91d117a5 -p mpv
		;;
	xa)
		acestream-launcher acestream://7cac3131958133ab8d363ebd3d15265ef46cbba9 -p mpv
		;;
	xba)
		acestream-launcher acestream://fbfbae118f5705fde5c217f88933b22f6aaf50a6 -p mpv
		;;
	*)
		acestream-launcher acestream://$1 -p mpv
		;;
	esac
}
