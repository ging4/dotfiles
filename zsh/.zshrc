# Colorful neofetch
#neofetch --ascii "$(cat '/home/tch0wk/Documents/Scripts/ascii/arch')" | lolcat -F 0.08 -t -S 20
#neofetch --ascii "$(cat '/home/tch0wk/Documents/Scripts/ascii/arch')" | lolcat -b
fastfetch

# Enable Powerlevel10k instant prompt.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# Colors
autoload -U colors && colors

PATH="$PATH:$(ruby -e 'print Gem.user_dir')/bin"
PATH="$HOME/.config/emacs/bin/:$PATH"

export EDITOR='nvim'
export GIT_EDITOR='nvim'
export ARCHFLAGS="-arch x86_64"
export MANPAGER='nvim +Man!'

# History options
export HISTSIZE=10000000
export SAVEHIST=$HISTSIZE
export HISTFILE="$HOME/.config/zsh/zsh_history"
setopt hist_ignore_all_dups
setopt hist_ignore_space
setopt hist_find_no_dups
setopt extended_history

# Misc options
setopt notify
setopt nobeep

# ZSH completion
zstyle ':completion:*' completer _expand _complete _ignored _correct _approximate
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list '' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}' 'r:|[._-.]=** r:|=**'
zstyle ':completion:*' max-errors 2 numeric
zstyle ':completion:*' menu select=5
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' fzf-search-display true
autoload -U compinit
compinit
_comp_options+=(globdots)

# Completion for kitty
#kitty + complete setup zsh | source /dev/stdin

# Window titles
autoload -Uz add-zsh-hook

function xterm_title_precmd () {
	print -Pn -- '\e]2;%n@%m %~\a'
	[[ "$TERM" == 'screen'* ]] && print -Pn -- '\e_\005{g}%n\005{-}@\005{m}%m\005{-} \005{B}%~\005{-}\e\\'
}

function xterm_title_preexec () {
	print -Pn -- '\e]2;%n@%m %~ %# ' && print -n -- "${(q)1}\a"
	[[ "$TERM" == 'screen'* ]] && { print -Pn -- '\e_\005{g}%n\005{-}@\005{m}%m\005{-} \005{B}%~\005{-} %# ' && print -n -- "${(q)1}\e\\"; }
}

if [[ "$TERM" == (Eterm*|alacritty*|aterm*|gnome*|konsole*|kterm*|putty*|rxvt*|screen*|tmux*|xterm*) ]]; then
	add-zsh-hook -Uz precmd xterm_title_precmd
	add-zsh-hook -Uz preexec xterm_title_preexec
fi

# Keybinds
bindkey "^[[H"	beginning-of-line
bindkey "^[[F"	end-of-line
bindkey "\e[1~" beginning-of-line
bindkey "\e[4~" end-of-line
bindkey "\e[7~" beginning-of-line
bindkey "\e[8~" end-of-line
bindkey "\eOH"	beginning-of-line
bindkey "\eOF"	end-of-line
bindkey "^[[3~"	delete-char
bindkey "^[[A"	history-substring-search-up
bindkey "^[[B"	history-substring-search-down

# Edit line keybind
autoload edit-command-line; zle -N edit-command-line
bindkey '^e' edit-command-line

# Sourcing theme, aliases and plugins
source /usr/share/zsh-theme-powerlevel10k/powerlevel10k.zsh-theme 2>/dev/null
[[ ! -f ~/.config/zsh/p10k.zsh ]] || source ~/.config/zsh/p10k.zsh

source ~/.config/zsh/aliasrc 2>/dev/null

source ~/.config/zsh/fzf-tab/fzf-tab.plugin.zsh 2>/dev/null
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh 2>/dev/null
source /usr/share/zsh/plugins/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh 2>/dev/null
source /usr/share/zsh/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh 2>/dev/null
export HISTORY_SUBSTRING_SEARCH_HIGHLIGHT_FOUND=bg=none
export HISTORY_SUBSTRING_SEARCH_HIGHLIGHT_NOT_FOUND=fg=none
