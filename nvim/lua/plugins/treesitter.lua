return {
    "nvim-treesitter/nvim-treesitter",
    opts = function(_, opts)
        if type(opts.ensure_installed) == "table" then
            vim.list_extend(
                opts.ensure_installed,
                { "css", "rust", "php", "json5", "jsonc", "zig", "poe_filter", "glsl", "c_sharp", "sql", "kotlin" }
            )
        end
        opts.highlight = {
            enable = true,
            additional_vim_regex_highlighting = { "markdown" },
        }
    end,
}
