return {
    "lukas-reineke/headlines.nvim",
    dependencies = "nvim-treesitter/nvim-treesitter",
    opts = {
        markdown = {
            headline_highlights = { "Headline1", "Headline2", "Headline3", "Headline4", "Headline5", "Headline6" },
            dash_string = "─",
            codeblock_highlight = false,
            fat_headlines = false,
            vim.cmd([[highlight Headline1 guifg=#ff4455 guibg=#250909]]),
            vim.cmd([[highlight Headline2 guifg=#ffdd33 guibg=#332415]]),
            vim.cmd([[highlight Headline3 guifg=#66dd66 guibg=#102510]]),
            vim.cmd([[highlight Headline4 guifg=#22aaff guibg=#101025]]),
            vim.cmd([[highlight Headline5 guifg=#6644ff guibg=#150928]]),
            vim.cmd([[highlight Headline6 guifg=#aa66ff guibg=#200923]]),
        },
    },
}
