local default_capabilities = {
    textDocument = {
        completion = {
            editsNearCursor = true,
        },
    },
    offsetEncoding = { "utf-32" },
}
return {

    "neovim/nvim-lspconfig",
    opts = {
        servers = {
            clangd = {
                capabilities = default_capabilities,
            },
        },
    },
}
