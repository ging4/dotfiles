return {
    "jose-elias-alvarez/null-ls.nvim",
    event = { "BufReadPre", "BufNewFile" },
    dependencies = { "mason.nvim" },
    opts = function()
        local nls = require("null-ls")
        return {
            root_dir = require("null-ls.utils").root_pattern(".null-ls-root", ".neoconf.json", "Makefile", ".git"),
            sources = {
                nls.builtins.formatting.stylua,
                nls.builtins.formatting.shfmt.with({
                    extra_args = { "-i", 4, "-ci", "-sr" },
                }),
                nls.builtins.diagnostics.flake8,
                nls.builtins.formatting.eslint_d,
                nls.builtins.diagnostics.eslint_d,
                nls.builtins.code_actions.eslint_d,
                nls.builtins.diagnostics.luacheck.with({
                    extra_args = { "--globals", "vim", "--std", "luajit" },
                }),
            },
        }
    end,
}
