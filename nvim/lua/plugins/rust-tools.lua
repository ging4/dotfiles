return {
    "simrat39/rust-tools.nvim",
    opts = {
        tools = {
            runnables = {
                use_telescope = true,
            },
            inlay_hints = {
                auto = true,
                show_parameter_hints = false,
                parameter_hints_prefix = "",
                other_hints_prefix = "",
            },
        },
        server = {
            -- on_attach is a callback called when the language server attachs to the buffer
            settings = {
                -- to enable rust-analyzer settings visit:
                -- https://github.com/rust-analyzer/rust-analyzer/blob/master/docs/user/generated_config.adoc
                ["rust-analyzer"] = {
                    -- enable clippy on save
                    checkOnSave = {
                        allFeatures = true,
                        command = "clippy",
                    },
                    cargo = {
                        allFeatures = true,
                    },
                    cmd = {
                        "rustup",
                        "run",
                        "stable",
                        "rust-analyzer",
                    },
                },
            },
        },
    },
}
