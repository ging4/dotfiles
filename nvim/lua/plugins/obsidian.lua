local wk = require("which-key")

return {
    "epwalsh/obsidian.nvim",
    lazy = true,
    event = { "BufReadPre " .. vim.fn.expand("~") .. "/Documents/Notes/**.md" },
    dependencies = {
        "nvim-lua/plenary.nvim",
        "hrsh7th/nvim-cmp",
        "nvim-telescope/telescope.nvim",
    },
    opts = {
        dir = "~/Documents/Notes/", -- no need to call 'vim.fn.expand' here
        daily_notes = {
            -- Optional, if you keep daily notes in a separate directory.
            folder = "Journal",
            -- Optional, if you want to change the date format for daily notes.
            date_format = "%Y-%m-%d",
        },
        templates = {
            subdir = "Templates",
            date_format = "%Y-%m-%d-%a",
            time_format = "%H:%M",
        },
        completion = {
            -- If using nvim-cmp, otherwise set to false
            nvim_cmp = true,
            -- Trigger completion at 2 chars
            -- min_chars = 1,
            -- Where to put new notes created from completion. Valid options are
            --  * "current_dir" - put new notes in same directory as the current buffer.
            --  * "notes_subdir" - put new notes in the default notes subdirectory.
            new_notes_location = "current_dir",
        },
        mappings = {
            -- Overrides the 'gf' mapping to work on markdown/wiki links within your vault.
            ["gf"] = wk.register(
                { gf = "<cmd>ObsidianFollowLink<CR>" },
                { mode = "n", noremap = "false", expr = "true" }
            ),
            -- ["gfb"] = wk.register(
            --     { gfb = "<cmd>ObsidianBacklinks<CR>" },
            --     { mode = "n", noremap = "false", expr = "true" }
            -- ),
            -- ["gfd"] = wk.register({ gfd = "<cmd>ObsidianToday<CR>" }, { mode = "n", noremap = "false", expr = "true" }),
        },
        open_app_foreground = false,
        follow_url_func = function(url)
            -- Open the URL in the default web browser.
            vim.fn.jobstart({ "kde-open", url }) -- linux
        end,
    },
    config = function(_, opts)
        require("obsidian").setup(opts)
        -- Optional, override the 'gf' keymap to utilize Obsidian's search functionality.
        -- see also: 'follow_url_func' config option below.
        vim.keymap.set("n", "gf", function()
            if require("obsidian").util.cursor_on_markdown_link() then
                return "<cmd>ObsidianFollowLink<CR>"
            else
                return "gf"
            end
        end, { noremap = false, expr = true })
    end,
}
