return {
    {
        "nvim-neo-tree/neo-tree.nvim",
        opts = {
            default_component_configs = {
                container = {
                    width = 30,
                    right_padding = 1,
                },
            },
            window = {
                width = 30,
            },
        },
    },
}
