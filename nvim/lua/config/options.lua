-- Options are automatically loaded before lazy.nvim startup
-- Default options that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/options.lua
-- Add any additional options here
local o = vim.opt
local g = vim.g
local f = vim.filetype

--
o.termguicolors = true
o.timeoutlen = 500
o.updatetime = 200
o.mouse = "a"

-- Number of screen lines to keep above and below the cursor
o.scrolloff = 8

-- Better editor UI
o.number = true
o.numberwidth = 2
o.relativenumber = false
-- o.winbar = "%=%m %f"
o.signcolumn = "yes:2"
o.cursorline = true

-- Better editing experience
o.expandtab = true
o.smarttab = true
o.cindent = true
o.autoindent = true
o.wrap = true
o.textwidth = 120
o.tabstop = 4
o.shiftwidth = 4
o.softtabstop = 4
o.list = true

-- Makes neovim and host OS clipboard play nicely with each other
o.clipboard = "unnamedplus"

-- Case insensitive searching UNLESS /C or capital in search
o.ignorecase = true

-- Undo and backup options
o.backup = true
o.writebackup = false
o.backupdir = "/home/tch0wk/.local/share/nvim/backup//"
o.undofile = true
o.undodir = "/home/tch0wk/.local/share/nvim/undo//"
o.swapfile = false
o.history = 50
o.splitright = true
o.splitbelow = true

-- Preserve view while jumping
o.jumpoptions = "view"

g.loaded_perl_provider = 0

-- Filetypes
f.add({
    extension = {
        mdx = "markdown",
        rc = "r",
        conf = "conf",
        jsp = "html",
        mjml = "html",
        tab = "guitartab",
    },
    pattern = {
        [".*%.env.*"] = "sh",
        ["ignore$"] = "conf",
    },
    filename = {
        ["yup.lock"] = "yaml",
        ["eslintrc"] = "json",
        ["prettierrc"] = "json",
    },
})
