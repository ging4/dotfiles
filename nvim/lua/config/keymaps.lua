-- Keymaps are automatically loaded on the VeryLazy event
-- Default keymaps that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/keymaps.lua
-- Add any additional keymaps here

vim.keymap.set(
    "n",
    "<leader>sx",
    require("telescope.builtin").resume,
    { noremap = true, silent = true, desc = "Resume" }
)

vim.keymap.set(
    "n",
    "<F2>",
    ":setlocal spell! spelllang=es,cjk<CR>",
    { noremap = true, silent = true, desc = "Spanish Spelling" }
)

vim.keymap.set(
    "n",
    "<F3>",
    ":setlocal spell! spelllang=en_us,cjk<CR>",
    { noremap = true, silent = true, desc = "English Spelling" }
)

vim.keymap.set(
    "n",
    "<F9>",
    ":silent update<Bar>silent !chromium '%:p'&<CR>",
    { noremap = true, silent = true, desc = "Opent current file on Chromium" }
)

-- Open compiler
vim.api.nvim_buf_set_keymap(0, "n", "<F6>", "<cmd>CompilerOpen<cr>", { noremap = true, silent = true })

-- Toggle compiler results
vim.api.nvim_buf_set_keymap(0, "n", "<S-F6>", "<cmd>CompilerToggleResults<cr>", { noremap = true, silent = true })
