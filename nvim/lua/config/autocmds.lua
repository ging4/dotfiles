-- Autocmds are automatically loaded on the VeryLazy event
-- Default autocmds that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/autocmds.lua
-- Add any additional autocmds here
--

local a = vim.api
local num_au = a.nvim_create_augroup("NUMTOSTR", { clear = true })

a.nvim_create_autocmd("TextYankPost", {
    group = num_au,
    callback = function()
        vim.highlight.on_yank({ higroup = "Visual", timeout = 1000 })
    end,
})

vim.cmd("autocmd FileType markdown set nospell")
